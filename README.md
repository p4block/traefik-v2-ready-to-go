## Out of the box Traefik 2 template

Sets up a fully operational reverse proxy, with examples on proxying a service and a port on the network.

Essentially "What should come as default" for Docker users. 
Runs on the host network and proxies any docker container that has the appropriate labels. 

Edit the configuration to add your desired domain and email for SSL cert notifications.

Then, paste the three magic labels on any container anywhere, change the name and domain and you are good to go!

